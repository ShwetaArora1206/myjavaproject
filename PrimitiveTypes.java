
public class PrimitiveTypes{
    public static void main(String[] args) {
        byte byte_value = 120;
        short short_value = 200;
        int integer_value = 120;
        long long_value = 22_00_00_000;

        char character_value = 's';
        
        boolean flag = true;
         
        float float_value=0.1f;
        double double_value=5.5;

        byte resize=(byte)integer_value;

        System.out.println("byte_value = "+byte_value);
        System.out.println("short_value = "+short_value);
        System.out.println("integer_value = "+integer_value);
        System.out.println("long_value = "+long_value);
        System.out.println("character_value = "+character_value);
        System.out.println("flag = "+flag);        
        System.out.println("float_value = "+float_value);
        System.out.println("double_value = "+double_value);
        System.out.println("resize = "+resize);
        
        

    }

}
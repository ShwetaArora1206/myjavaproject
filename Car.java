public class Car{
    private String model;
    private String make;
    private int speed;
    private String colour;

    public void accelerate(){
        speed++;
    }

    public void slowdown(){
        speed--;
    }

    public void stop(){
        speed=0;
    }
     public int getcurrentspeed(){
         return speed;
     }
    
}
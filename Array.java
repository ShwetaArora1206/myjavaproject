class Array{
        static int initialvalue = 10;
        static int[][] intArray = new int[4][4];
    public static void main(final String[] args) {
        // int [] intArray = new int[10];
        // int[] intArray = new int[] { 00, 11, 22, 33, 44, 55 };
        
        populateArray();
        printArray();

    }

    public static void populateArray() {
        for(int indexi=0;indexi<intArray.length;indexi++){
            for(int indexj=0;indexj<intArray[0].length;indexj++){
                intArray[indexi][indexj]=initialvalue;
                initialvalue++;
            }
        }
    }

    public static void printArray() {
            for(int indexi=0;indexi<intArray.length;indexi++){
                for(int indexj=0;indexj<intArray[0].length;indexj++){
                    System.out.print(intArray[indexi][indexj]+" ");
                }
                System.out.println();
            }
    }
}
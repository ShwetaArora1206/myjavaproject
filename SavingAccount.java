public class SavingAccount{
    private String name;
    private double accountbal;
    private static long counter=1000;
    private long acc_Id;
    
    SavingAccount(String name, double accountbal){
        this.name=name;
        this.accountbal=accountbal;
        this.acc_Id=counter++;

    }
    SavingAccount(String name){
        this.name=name;
        this.acc_Id=counter++;
    }

    public void deposit(int amt){
        accountbal=accountbal+amt;
    }
    public void deposit(int amt, String S){
        accountbal=accountbal+amt;
        System.out.println(S);
    }
    public double withdraw(int amt){
        if(amt<accountbal)
            accountbal=accountbal-amt;
        else
            System.out.println("Insufficient Balance");
    return amt; 
    }
    public double withdraw(int amt,int S){
        if(amt<accountbal){
            accountbal=accountbal-amt;
        else
            System.out.println("Insufficient Balance");

        System.out.println(S);
    return amt; 
    }
    public double showbal(){
       return this.accountbal;
    }
    public String getName(){
        return this.name;
    }
    public long getAccId(){
        return this.acc_Id;
    } 
}